# Super Simple Demo Protractor

Proyecto muy básico para probar las funcionalidades más básicas de protractor en modo standalone, con el navegador Google Chrome, sin estar acoplado a ningún framework por debajo (Angular, React, Vue.js, etc.) y en javascript puro.

## Instalación proyecto

``` bash
npm i
```

## Ejecución tests de prueba

``` bash
npm run e2e
```

Al ejecutarlo al final obtendremos un mensaje similar al siguiente:

``` bash
2 spec, 0 failures
Finished in 6.246 seconds

[18:02:14] I/launcher - 0 instance(s) of WebDriver still running
[18:02:14] I/launcher - Google Chrome passed
```
