exports.config = {
    framework: 'jasmine2',
    specs: ['./src/*.spec.js'],
    multiCapabilities: [{
        browserName: 'chrome',
        logName: 'Google Chrome',
        chromeOptions: {
            args: [
            // '--headless',
            // '--disable-gpu',
            '--window-size=1366,720',
            'lang=es-ES'
            ]
        }
    }],
    params: {
        device: 'chrome'
    },
    directConnect: true
};
